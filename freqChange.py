#!/usr/bin/env python3

import subprocess
import zipfile
import inspect
import os
import sys
import shutil

ORIG_FILE_NAME = 'battleDamageIndicatorApp.swf'
SWFDECOMPRESS = 'swfdecompress.exe'
SWF7ZCOMPRESS = 'swf7zcompress.exe'
PATH_RABCDASM = 'D:/RABCDAsm_v1.18/'
# PATH_WOT = 'D:/World_of_Tanks/'
# PATH_GUI_PKG_1 = PATH_WOT + 'res/packages/gui-part1.pkg'
# PATH_GUI_PKG_2 = PATH_WOT + 'res/packages/gui-part2.pkg'
PATH_GUI_PKG_1 = '{}/res/packages/gui-part1.pkg'
PATH_GUI_PKG_2 = '{}/res/packages/gui-part2.pkg'
PATH_SWF = 'gui/flash/' + ORIG_FILE_NAME
NEW_FREQUENCY = 10


def get_script_dir(follow_symlinks=True):
    if getattr(sys, 'frozen', False):  # py2exe, PyInstaller, cx_Freeze
        path = os.path.abspath(sys.executable)
    else:
        path = inspect.getabsfile(get_script_dir)
    if follow_symlinks:
        path = os.path.realpath(path)
    return os.path.dirname(path)


# changing the frequency
def changingFrequency():
    result = subprocess.call([os.path.join(PATH_RABCDASM, SWFDECOMPRESS), ORIG_FILE_NAME])
    if result <= 0:
        with open(ORIG_FILE_NAME, 'rb+') as f:
            f.seek(18)
            f.write(bytearray([NEW_FREQUENCY]))
        result = subprocess.call([os.path.join(PATH_RABCDASM, SWF7ZCOMPRESS), ORIG_FILE_NAME])

    if result > 0:
        print('Error = %s' % result)


# extract the SWF
def extractSWF(path_wot):
    with zipfile.ZipFile(PATH_GUI_PKG_1.format(path_wot), 'r') as zf:
        src = zf.open(PATH_SWF)
        if src is not None:
            trg = open(os.path.join(get_script_dir(), ORIG_FILE_NAME), "wb")
            with src, trg:
                shutil.copyfileobj(src, trg)

    if src is None:
        with zipfile.ZipFile(PATH_GUI_PKG_2.format(path_wot), 'r') as zf:
            src = zf.open(PATH_SWF)
            if src is not None:
                target = open(os.path.join(get_script_dir(), ORIG_FILE_NAME), "wb")
                with src, target:
                    shutil.copyfileobj(src, target)
    

# try:
#     input()
# except EOFError:
#     pass
