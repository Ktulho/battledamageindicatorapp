from xml.dom import minidom
import re


def getVersion(file_name):
    dom = minidom.parse(file_name)
    dom.normalize()
    node1 = dom.getElementsByTagName("version")
    ver = node1[0].childNodes[0].nodeValue
    return re.search(r'(\.\d+)+', ver).group(0)[1:]


# print(getVersion(r'D:\World_of_Tanks\version.xml'))
