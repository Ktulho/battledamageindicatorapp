# Developed on the basis of https://habr.com/ru/company/ruvds/blog/451872/

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
from freqChange import changingFrequency, extractSWF
from versionWOT import getVersion


CHROMEDRIVER_PATH = 'chromedriver.exe'
SWF = 'battleDamageIndicatorApp.swf'
SITE_ADDRESS = 'https://kr.cm/f/t/36620/'
TOPIC_ADDRESS = '{}?do=edit'.format(SITE_ADDRESS)
FILE_CONTAINER = '//ul[@data-role="fileContainer"]'
FILE_TITLE = '//*[@data-role="title"]'
SELECT_FILE = '//*[@id="elEditorDrop_topic_content_upload"]/div/ul/li[1]/div/input'
COMMENT_CONTAINER = '//div[@class="ipsComposeArea_editor"]'
PATH = r'D:\My\Programming\Python\damageIndicator\battleDamageIndicatorApp.swf'
CHANGE_TOPIC = '//*[@id="ipsLayout_mainArea"]/form/div/div[3]/button'
PATH_SWF = '\\res_mods\\Х.Х.Х.Х\\gui\\flash\\'
PATH_WOT = r'D:\World_of_Tanks'
VERSION_FILE = r'{}\version.xml'.format(PATH_WOT)
CHECK_DONT_MERGE = r'//*[@id="check_dontmerge_wrapper"]'
COMMENT = 'Обновил battleDamageIndicatorApp.swf в шапке для WoT {}.'

chromeOptions = Options()
chromeOptions.add_argument(r'--user-data-dir=D:\My\Programming\Python\damageIndicator\chrome')
chromeOptions.add_argument(r'--profile-directory=Profile 1')


def indexSWF(files):
    for i, file in enumerate(files):
        if file.text == SWF:
            return i + 1
    else:
        return 0


extractSWF(PATH_WOT)
changingFrequency()

# Open a page
driver = webdriver.Chrome(executable_path=CHROMEDRIVER_PATH, options=chromeOptions)
driver.get(TOPIC_ADDRESS)

# Delete a file
index = indexSWF(driver.find_elements_by_xpath(FILE_TITLE))
if index:
    print(driver.find_element_by_xpath('{}/li[{}]{}'.format(FILE_CONTAINER, index, '/div[2]/strong')).text)
    driver.find_element_by_xpath('{}/li[{}]{}'.format(FILE_CONTAINER, index, '/div[3]/ul/li[2]/a')).click()

# Insert a file
driver.find_element_by_xpath(SELECT_FILE).send_keys(PATH)
text = driver.find_element_by_xpath('//strong[contains(text(),"{}")]'.format(PATH_SWF))
text.click()
text.send_keys(Keys.DOWN)
time.sleep(2)
index = indexSWF(driver.find_elements_by_xpath(FILE_TITLE))
if index:
    print(driver.find_element_by_xpath('{}/li[{}]{}'.format(FILE_CONTAINER, index, '/div[2]/h2')).text)
    driver.find_element_by_xpath('{}/li[{}]{}'.format(FILE_CONTAINER, index, '/div[4]/ul/li[1]/a')).click()

# Change the topic
driver.find_element_by_xpath(CHANGE_TOPIC).click()
time.sleep(1)

# Add a comment
driver.get(SITE_ADDRESS)
driver.find_element_by_xpath('{}'.format(COMMENT_CONTAINER)).click()
time.sleep(1)
ver = COMMENT.format(getVersion(VERSION_FILE))
driver.find_element_by_xpath('{}/div/div[1]/div[1]/div/div/div/div'.format(COMMENT_CONTAINER)).clear()
driver.find_element_by_xpath('{}/div/div[1]/div[1]/div/div/div/div'.format(COMMENT_CONTAINER)).send_keys(ver)
driver.find_element_by_xpath('{}'.format(CHECK_DONT_MERGE)).click()
time.sleep(1)
driver.find_element_by_xpath('{}'.format('//*[@id="ipsLayout_mainArea"]/div[3]/div[5]/form/div/div[2]/ul/li[3]/button')).click()

time.sleep(1)
driver.close()
