## Описание

Данный проект предназначен для автоматизации обновления и публикации файла **battleDamageIndicatorApp.swf** из ресурсов игры **World of Tanks**.

## В проекте использовались

- [ChromeDriver](https://chromedriver.chromium.org/)
- [RABCDAsm](https://github.com/CyberShadow/RABCDAsm)
